#!/usr/bin/env python

"""get-fliqz.py: Download fliqz videos in mp4 format"""

__author__    = 'masonlabs'
__copyright__ = 'Copyright (C) 2018 masonlabs <git@masonlabs.org>'
__license__   = 'MIT'
__version__   = '0.1.0'

import argparse
import logging
import os
import re
import sys
import urllib2
from HTMLParser import HTMLParser

class FliqzParser(HTMLParser):
  links = []
  h1 = []
  h1_flag = False
  desc = []
  desc_flag = False

  def handle_starttag(self, tag, attrs):
    if tag == 'script':
      attrs = dict(attrs)
      self.links.append(attrs.get('src'))
    if tag == 'h1':
      self.h1_flag = True
    if tag == 'p':
      attrs = dict(attrs)
      if attrs.has_key('id') and attrs.get('id') == 'description':
        self.desc_flag = True

  def handle_endtag(self, tag):
    if tag == 'h1':
      self.h1_flag = False
    if tag == 'p':
      self.desc_flag = False

  def handle_data(self, data):
    if self.h1_flag:
      self.h1.append(data)
    if self.desc_flag:
      self.desc.append(data)

  def get_script_link(self):
    if self.links.count > 0:
      return(re.sub(r'\?.*', '', self.links[0]))
    return None

  def get_title(self):
    if self.h1.count > 0:
      return(re.sub(r'\.mp4$', '', self.h1[0]))
    return None

  def get_desc(self):
    if self.desc.count > 0:
      return(self.desc[0])
    return None

class Fliqz:

  def __init__(self,):
    ## mobile header required to get mp4 video format
    self.user_agent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1'

  def get_url(self, url):
    headers = {'User-Agent': self.user_agent}
    logger.debug("get url: %s", url)
    request = urllib2.Request(url, None, headers)
    response = urllib2.urlopen(request)
    return response.read()

  def sanitize_filename(self, filename):
    filename = re.sub(r'\s+', '-', filename)
    filename = re.sub(r'_', '-', filename)
    filename = re.sub(r'-+', '-', filename)
    filename = re.sub(r'[^a-zA-Z0-9_-]+', '', filename)
    filename = filename.lower() + '.mp4'
    return filename

argparser = argparse.ArgumentParser(description='Download fliqz videos in mp4 format')
argparser.add_argument('-d', '--debug', action="store_true",
                    help="enable debug logging; display all fliqz page content")
argparser.add_argument('--desc', action="store_true",
                    help="use page description for mp4 filename, instead of fliqz-generated filename")
argparser.add_argument('--dry-run', action="store_true",
                    help="dry-run mode; download html/javascript content, but not mp4 content")
argparser.add_argument('-t', '--title', action="store_true",
                    help="use page title for mp4 filename, instead of fliqz-generated filename")
argparser.add_argument('-u', '--url', required=True,
                    help='fliqz permalink url (see README for examples)')

opts = argparser.parse_args()
url = opts.url
filename = None

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')
logger = logging.getLogger(os.path.basename(__file__))

if opts.debug:
  logger.setLevel(logging.DEBUG)

if opts.title and opts.desc:
  sys.exit("Please specify either --title OR --desc")

logger.info("fliqz url: %s", url)

fliqz = Fliqz()
content = fliqz.get_url(url)

logger.debug('fliqz content: %s', content)

parser = FliqzParser()
parser.feed(content)

script_link = parser.get_script_link()

if script_link is None:
  logger.error("Unable to find javascript src on fliqz url")
  sys.exit(1)

logger.info("found javascript src: %s", parser.get_script_link())

if opts.title and parser.get_title():
  title = parser.get_title()
  logger.info("found title: %s", title)
  filename = fliqz.sanitize_filename(title)
  logger.info("using mp4 filename: %s", filename)

if opts.desc and parser.get_desc():
  desc = parser.get_desc()
  logger.info("found description: %s", desc)
  filename = fliqz.sanitize_filename(desc)
  logger.info("using mp4 filename: %s", filename)

content = fliqz.get_url(parser.get_script_link())
logger.debug('fliqz javascript content: %s', content)

m = re.findall("(?P<url>http?://[^\s]+\.mp4)", content)

logger.info("found mp4 url: %s", m[0])

if not opts.dry_run:

  logger.info("downloading mp4 with curl")

  if filename is None:
    logger.info("output filename: %s", os.path.basename(m[0]))
    os.execl("/usr/bin/curl", '-v', '-H', 'User-Agent:{}'.format(fliqz.user_agent), '-O', m[0])
  else:
    logger.info("output filename: %s", filename)
    os.execl("/usr/bin/curl", '-v', '-H', 'User-Agent:{}'.format(fliqz.user_agent), '-o', filename, m[0])
